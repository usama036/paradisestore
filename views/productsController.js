const Appointment = require('../models/appointment.model.js');
const express = require('express')
var router = express.Router()
var Admin=require("./admin")
var Category=require("./category")
// create and save
router.post('/create', async (req, res) => {
  // Validate request
     try{
     
   
    


    
    

     // Create an Appointment
     const appointment = new Category({
        
        category:req.body.category,
     });

     // Save Appointment in the database
     await appointment.save()
     .then(data => {
      return res.status(201).send(responseGenerator(true, data, ''))
     }).catch(err => {
      return res.status(400).send(responseGenerator(false, "No " + "amy thing found .", '', ''))
     })
    }catch{
        return res.status(200).send(responseGenerator(false, "Something went wrong" , "", ))


    }


});




router.post('/login',async (req, res) => {
    try{
   
 Appointment.findOne({email:req.body.email}).exec().then(record=>{
     if(!record){

        return res.status(201).send(responseGenerator(false, "email not found", ''))

     }

if(record){
console.log(record)
if(record.password==req.body.password){

    const payload={
        id:record._id,
        email:record.email
    }
    jwt.sign(payload, config.secret,{
        expiresIn: 14400
    }, (err, token) => { 
        if(err) {
            return res.status(400).send(responseGenerator(false, "No token found " + " ", '', ''))
}

res.json({
    status: true,
    data: record,
    token: `Bearer ${token}`
});

})
}else {
    return res.status(200).send(responseGenerator(false, "password not match"  ))
}


}


else{
    return res.status(200).send(responseGenerator(false, "No Record found"  ))

}

  })
 } catch(err){
    return res.status(400).send(responseGenerator(false, "No " + "amy thing found .", '', ''))

}


 })








// Retrieve and return all
router.get('/appointments', isAuthenticated,(req, res) => {

    
      Appointment.find()
    .then(appointments => {
        return res.status(200).send(responseGenerator(true, "your record", appointments ))

    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving appointments."
        });
    });
});

// Find one by ID
router.get('/appointments/:appointmentId',(req, res) => {
  Appointment.findById(req.params.appointmentId)
   .then(appointment => {
       if(!appointment) {
           return res.status(404).send({
               message: "Appointment not found with id " + req.params.appointmentId
           });
}
return res.status(201).send(responseGenerator(true, "data found", appointment))


   }).catch(err => {
       if(err.kind === 'ObjectId') {
           return res.status(404).send({
               message: "Appointment not found with id " + req.params.appointmentId
           });
       }
       return res.status(500).send({
           message: "Error retrieving appointment with id " + req.params.appointmentId
       });
   });
});

// Update a appointment identified by the appointmentId in the request
router.post('/appointments/:appointmentId',(req, res) => {


    // Validate Request
    if(!req.body.email) {
        return res.status(400).send({
            message: "Appointment content can not be empty"
        });
    }
    // Find appointment and update it with the request body
    Appointment.findByIdAndUpdate(req.params.appointmentId, {
        email: req.body.email ,
        password: req.body.password,
        select: req.body.select,
        address: req.body.address,
        phone: req.body.phone,
        city:req.body.city,
        zip:req.body.zip,
    }, {new: true})
    .then(appointment => {
        if(!appointment) {
            return res.status(404).send({
                message: "Appointment not found with id " + req.params.appointmentId
            });
        }
        return res.status(201).send(responseGenerator(true,  "update", appointment))
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(200).send(responseGenerator(false, "not update"  ))

        }
        return res.status(500).send({
            message: "Error updating appointment with id " + req.params.appointmentId
        });
    });
});

// Delete
router.delete('/appointments/:appointmentId', (req, res) => {
  Appointment.findByIdAndRemove(req.params.appointmentId)
     .then(appointment => {
         if(!appointment) {
             return res.status(404).send({
                 message: "Appointment not found with id " + req.params.appointmentId
             });
         }
         return res.status(201).send(responseGenerator(true, 'Appointmend deleted', ''))
        }).catch(err => {
         if(err.kind === 'ObjectId' || err.name === 'NotFound') {
             return res.status(404).send({
                 message: "Appointment not found with id " + req.params.appointmentId
             });
         }
         return res.status(500).send({
             message: "Could not delete appointment with id " + req.params.appointmentId
         });
     });
});


module.exports=router