const express = require('express')
const app = express()
var User = require('./module')
var bodyParser = require('body-parser')
const bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var config = require('./config');
var auth = require('./auth')
const ejs = require("ejs")
app.set('view engine', 'ejs');
app.use(express.static('views'));
var Admin=require("./admin")
app.use("/uploads",express.static('uploads'));
var multer = require('multer')
let Category=require('./category')

//********************************image upload**************************************

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
      cb(null,Date.now()+"__"+file.originalname )
    }
  })
   

  var upload = multer({ storage: storage}).single('productImage')
//***********************************token verifying***********************************

if (typeof localStorage === "undefined" || localStorage === null) {
    var LocalStorage = require('node-localstorage').LocalStorage;
    localStorage = new LocalStorage('./scratch');
  }


var checkauth=function(req,res,next){
var mytoken=localStorage.getItem('mytoken')
console.log(mytoken)

jwt.verify(mytoken, config.secret, function(err, decoded) {
    if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' })
 
next()
})}


var checkAdmin=function(req,res,next){
    var mytoken=localStorage.getItem('mytoken')
    
    
    jwt.verify(mytoken, config.secret, function(err, decoded) {
        if (err) {res.render('login')}
     
    next()
    })}



app.use(bodyParser.urlencoded({
    extended: false
}))

app.get('/', (req, res) => {
    res.render("index", {})
})

app.get('/admin',checkAdmin,async (req, res) => {
let record=await Admin.find()
console.log( record.length)
    res.render("admin", {data:record.length})
})


app.get('/signup', function (req, res) {
    res.render("signup", {})
})

app.post('/', function (req, res) {



    User.find({
        email: req.body.email
    }).exec().then(user => {

        if (user.length >= 1) {

            var usama = {
                message: "already exist"
            }

            res.render('signup', {
                expressFlash: usama.message
            })
        } else {
            bcrypt.hash(req.body.pass, 10, function (err, hash) {


                if (err) {

                    res.status(500).json({
                        error: err
                    })
                } else {

                    const user = new User({
                        Name: req.body.username,
                        City: req.body.city,
                        email: req.body.email,
                        password: hash

                    })
                    user.save()
                        .then(result => {

                            res.redirect("/")
                        })
                        .catch(err => {
                            res.status(500).json({
                                error: err
                            })
                        })

                }




            })

        }



    })
})



app.post('/login', (req, res) => {

    User.find({
            email: req.body.email
        }).exec().then(user => {


            if (user.length < 1) {
                res.status(404).json({
                    message: "mail not found"
                })
            }


            bcrypt.compare(req.body.password, user[0].password, function (err, result) {


                if (err) {
                    res.status(404).json({
                        message: "authfailed"
                    })
                }


                if (result) {

                    var token = jwt.sign({
                        email: user[0].email,
                        id: user[0]._id
                    }, config.secret, {
                        expiresIn: "86400"
                    });

                    localStorage.setItem('mytoken', token)


                    // res.status(400).json({
                    //     message: "Auth successfull",
                    //     token: token
                    // })

                    res.redirect("/admin")


                } else {
                    res.status(500).json({
                        error: 'password dost noooooooot matvh'
                    })
                }
            });
        })

        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
})


app.get('/home', function (req, res) {

res.render("index")

})


app.get("/logout",function(req,res){
    
    localStorage.removeItem('mytoken');
res.redirect("/")
})

app.get("/addProduct",checkAdmin, async function(req,res){
    var user= await Category.find()

    res.render('product',{data:user})
})

app.get('/men',(req,res)=>{
   

    Admin.find().exec().then(data=>{
       res.render("men",{data:data})
    })
   
   
})


app.post('/add',(req,res)=>{
   console.log(req.body)

    res.end()
    })
   
   
    app.post('/homeImage',upload,(req,res)=>{
   
  
        var user= new Admin({image:req.file.path,title:req.body.title,
            price:req.body.price,
            discount:req.body.discount,
            description:req.body.description,
            category:req.body.category});
        user.save().then(data=>{
        res.status(200).json({data})
            })
       .catch(err=>{
            res.status(404).json({
                message:err
    
            })
        })
    res.end()
    })


app.post('/menimage',(req,res)=>{
   
console.log(req.file)
//     var user= new Admin({image:req.file.path});
//     user.save().then(data=>{
//     res.status(200).json({message:"image save successfull"})
//         })
//    .catch(err=>{
//         res.status(404).json({
//             message:err

//         })
//     })
})



app.post('/createCategory',  (req,res) => {
    console.log(req.body.category)
    var user= new Category({category:req.body.category});
    user.save().then(data=>{
   
        res.redirect('/category')
        })
   .catch(err=>{
        res.status(404).json({
            message:err

        })
    })
  
  });

  app.get('/getCategory', async (req,res) => {
    var user= await Category.find()
   
  
    res.render('product',{data:user})
   
        })
   

  
        app.get('/category', async (req,res) => {
            
            var user= await Category.find()
   
  
    res.render('category',{data:user})
            
           
                })



                app.get('/delete/:id', async (req,res) => {
            
                    var user= await Category.findByIdAndDelete({_id:req.params.id})
           
          
            res.redirect('/category')
                    
                   
                        })

port=process.env.PORT || 3000
app.listen(port,function(){console.log(`${port}`)})
